if has('nvim')
  let s:editor_root=expand("~/.config/nvim")
else
  let s:editor_root=expand("~/.vim")
endif

call plug#begin(s:editor_root . '/plugged')

"Plug 'jalvesaq/Nvim-R'
"Plug 'jalvesaq/R-Vim-runtime'
Plug 'jcfaria/Vim-R-plugin'
Plug 'kshenoy/vim-signature'
Plug 'jalvesaq/southernlights'
Plug 'chrisbra/csv.vim'
Plug 'vim-scripts/Vimball'
Plug 'chazy/cscope_maps'

Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'
Plug 'craigemery/vim-autotag'

Plug 'Valloric/YouCompleteMe', {'do': './install.py --clang-completer --gocode-completer --tern-completer'}
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}

call plug#end()

set nocompatible
syntax enable
filetype plugin indent on

"------------------------------------
" Behavior
"- ----------------------------------
let maplocalleader = ","
let mapleader = ";"
let r_syntax_folding = 1
let R_assign = 0

"------------------------------------
" Appearance 
"- ----------------------------------
colorscheme southernlights
if &t_Co == 256
    let rour_input    = 'ctermfg=247'
    let rout_color_normal   = 'ctermfg=202'
    let rout_color_number   = 'ctermfg=214'
    let rout_color_integer  = 'ctermfg=214'
    let rout_color_float    = 'ctermfg=214'
    let rout_color_complex  = 'ctermfg=214'
    let rout_color_negnum   = 'ctermfg=209'
    let rout_color_negfloat = 'ctermfg=209'
    let rout_color_date     = 'ctermfg=184'
    let rout_color_true     = 'ctermfg=78'
    let rout_color_false    = 'ctermfg=203'
    let rout_color_inf      = 'ctermfg=39'
    let rout_color_constant = 'ctermfg=179'
    let rout_color_string   = 'ctermfg=172'
    let rout_color_error    = 'ctermfg=15 ctermbg=1'
    let rout_color_warn     = 'ctermfg=1'
    let rout_color_index    = 'ctermfg=186'
endif


set sw=2
set tabstop=4
set number

"------------------------------------
" Search
"------------------------------------

set infercase
set hlsearch
set incsearch

"------------------------------------
" Nvim-R
"------------------------------------
if has("gui_running")
  inoremap <C-Space> <C-x><C-o>
else
  inoremap <Nul> <C-x><C-o>
endif
vmap <Space> <Plug>RDSendSelection
nmap <Space> <Plug>RDSendLine

autocmd FileType c nnoremap <buffer> <silent> <C-]> :YcmCompleter GoTo<cr>

